// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements. See the NOTICE file distributed with this
// work for additional information regarding copyright ownership. The ASF
// licenses this file to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <new>
#include <vector>
#include "parser.h"
#include "parser.hpp"

void* parserAlloc(void *(*)(size_t));
void parserFree(void*, void (*)(void*));
void parser(void*, int, token*, context*);
void parserTrace(FILE*, char*);

extern char* optarg;
extern int   optind;

%%{
  machine scanner;

  prepush { stack.push_back(0); }
  postpop { stack.pop_back(); }

  number
    = (digit @{ number *= 10; number += fc - '0'; }
      )+
    >{ number = 0; };

  xnumber
    = ( [0-9] @{ number <<= 4; number |= fc - '0'; }
      | [a-f] @{ number <<= 4; number |= fc - 'a' + 0x0a; }
      | [A-F] @{ number <<= 4; number |= fc - 'A' + 0x0a; }
      )+
    >{ number = 0; };

  expression := |*
    [^\]]+ => { parse(TK_EXPRESSION, ts, te); };

    "]" => {
      parse(TK_RIGHT_SQUARE_BRACKET);
      fret;
    };
  *|;

  main := |*
    [\t\v\f ]+;

    '//' [^\r\n]*;

    "\r\n" | "\r" | "\n" => {
      ++line_;
    };

    ".get"     => { parse(TK_MACRO_GET); };
    ".set"     => { parse(TK_MACRO_SET); };
    ".import"  => { parse(TK_MACRO_IMPORT); };
    ".pack"    => { parse(TK_MACRO_PACK); };
    ".unpack"  => { parse(TK_MACRO_UNPACK); };
    ".uint32"  => { parse(TK_MACRO_UINT32); };
    ".uint64"  => { parse(TK_MACRO_UINT64); };
    ".debug"   => { parse(TK_MACRO_DEBUG); };
    "template" => { parse(TK_TEMPLATE); };
    "if"       => { parse(TK_IF); };
    "then"     => { parse(TK_THEN); };
    "else"     => { parse(TK_ELSE); };
    "end"      => { parse(TK_END); };

    ".expression[" => {
      parse(TK_MACRO_EXPRESSION);
      parse(TK_LEFT_SQUARE_BRACKET);
      fcall expression;
    };

    [a-zA-Z\-_] [0-9a-zA-Z\-_]* => {
      parse(TK_IDENTIFIER, ts, te);
    };

    "$" [a-zA-Z\-_] [0-9a-zA-Z\-_]* => {
      parse(TK_VARIABLE, ts + 1, te);
    };

    "@" [a-zA-Z\-_] [0-9a-zA-Z\-_]* => {
      parse(TK_REGISTER, ts + 1, te);
    };

    number => {
      parse(TK_NUMBER, number);
    };

    "0x" xnumber => {
      parse(TK_NUMBER, number);
    };

    "("  => { parse(TK_LEFT_PARENTHESIS); };
    ")"  => { parse(TK_RIGHT_PARENTHESIS); };
    ","  => { parse(TK_COMMA); };
    "="  => { parse(TK_EQUALS_SIGN); };
    "=>" => { parse(TK_WITH); };
    "["  => { parse(TK_LEFT_SQUARE_BRACKET); };
    "]"  => { parse(TK_RIGHT_SQUARE_BRACKET); };
  *|;
}%%

%%write data;

class generator {
public:
  explicit generator(bool verbose)
    : parser_(parserAlloc(malloc)), line_(1) {
    if (!parser_) {
      throw std::bad_alloc();
    }
    if (verbose) {
      char prompt[4] = { '>', '>', ' ', 0 };
      parserTrace(stderr, prompt);
    }
  }

  ~generator() {
    parserFree(parser_, free);
  }

  inline void parse(int major) {
    parser(parser_, major, new token(line_), &ctx_);
  }

  inline void parse(int major, const char* begin, const char* end) {
    parser(parser_, major, new token(std::string(begin, end), line_), &ctx_);
  }

  inline void parse(int major, uintmax_t minor) {
    parser(parser_, major, new token(minor, line_), &ctx_);
  }

  inline void generate(const char* p, const char* pe) {
    int cs;
    const char* eof = pe;
    int act;
    const char* ts;
    const char* te;
    int top;
    std::vector<int> stack;

    uintmax_t number = 0;
    line_ = 1;

    %%write init;
    %%write exec;
    parse(0);

    if (cs == %%{ write error; }%%) {
      std::cerr << "cs == error\n";
    } else if (cs < %%{ write first_final; }%%) {
      std::cerr << "cs < first_final\n";
    }
  }

private:
  void* parser_;
  context ctx_;
  int line_;

  generator(const generator&);
  generator& operator=(const generator&);
};

inline void print_help(std::ostream& out, const char* program) {
  out << "SYNOPSIS\n"
      << "  " << program << " [-v] <filename>\n"
      << "  " << program << " -h\n"
      << "\n"
      << "OPTIONS\n"
      << "  -h  Print a description of the command options.\n"
      << "  -v  Turn on the verbose mode.\n"
      << "\n";
}

int main(int argc, char* argv[]) {
  bool verbose = false;
  while (true) {
    int c = getopt(argc, argv, "hv");
    if (c == -1) {
      break;
    }
    switch (c) {
      case 'h':
        print_help(std::cout, argv[0]);
        return 0;
      case 'v':
        verbose = true;
        break;
      case '?':
        print_help(std::cout, argv[0]);
        return 1;
    }
  }
  if (optind == argc) {
    print_help(std::cout, argv[0]);
    return 1;
  }

  std::vector<char> source;
  {
    std::ifstream in(argv[optind]);
    if (!in) {
      std::cerr << "Could not open " << argv[optind] << "\n";
      return 1;
    }

    in.seekg(0, std::ios::end);
    source.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read(&source[0], source.size());
  }

  generator(verbose).generate(&source[0], &source[0] + source.size());
  return 0;
}

// vim: filetype=ragel
