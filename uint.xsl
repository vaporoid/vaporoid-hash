<?xml version="1.0" encoding="UTF-8"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements. See the NOTICE file distributed with this
  work for additional information regarding copyright ownership. The ASF
  licenses this file to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  License for the specific language governing permissions and limitations under
  the License.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="uint_.xsl"/>

  <xsl:template name="uint16-decode">
    <xsl:param name="byte"/>
    <xsl:variable name="upper">
      <xsl:call-template name="uint8-decode">
        <xsl:with-param name="byte" select="substring($byte, 3, 2)"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="lower">
      <xsl:call-template name="uint8-decode">
        <xsl:with-param name="byte" select="substring($byte, 1, 2)"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="$upper * 256 + $lower"/>
  </xsl:template>

  <xsl:template name="uint16-encode">
    <xsl:param name="word"/>
    <xsl:variable name="upper">
      <xsl:call-template name="uint8-encode">
        <xsl:with-param name="word" select="floor($word div 256)"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="lower">
      <xsl:call-template name="uint8-encode">
        <xsl:with-param name="word" select="$word mod 256"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="concat($lower, $upper)"/>
  </xsl:template>

  <xsl:template name="uint32-decode">
    <xsl:param name="byte"/>
    <xsl:variable name="upper">
      <xsl:call-template name="uint16-decode">
        <xsl:with-param name="byte" select="substring($byte, 5, 4)"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="lower">
      <xsl:call-template name="uint16-decode">
        <xsl:with-param name="byte" select="substring($byte, 1, 4)"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="format-number($upper * 65536 + $lower, '0')"/>
  </xsl:template>

  <xsl:template name="uint32-encode">
    <xsl:param name="word"/>
    <xsl:variable name="upper">
      <xsl:call-template name="uint16-encode">
        <xsl:with-param name="word" select="floor($word div 65536)"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="lower">
      <xsl:call-template name="uint16-encode">
        <xsl:with-param name="word" select="$word mod 65536"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="concat($lower, $upper)"/>
  </xsl:template>

  <xsl:template name="uint32-bitwise-not">
    <xsl:param name="a"/>
    <xsl:value-of select="format-number(4294967295 - $a, '0')"/>
  </xsl:template>

  <xsl:template name="uint32-left-shift-n">
    <xsl:param name="a"/>
    <xsl:param name="b"/>
    <xsl:choose>
      <xsl:when test="$b = 0">
        <xsl:value-of select="$a"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="uint32-left-shift-n">
          <xsl:with-param name="a" select="format-number($a * 2 mod 4294967296, '0')"/>
          <xsl:with-param name="b" select="$b - 1"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="uint32-right-shift-n">
    <xsl:param name="a"/>
    <xsl:param name="b"/>
    <xsl:choose>
      <xsl:when test="$b = 0">
        <xsl:value-of select="$a"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="uint32-right-shift-n">
          <xsl:with-param name="a" select="format-number(floor($a div 2), '0')"/>
          <xsl:with-param name="b" select="$b - 1"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="uint32-add">
    <xsl:param name="a"/>
    <xsl:param name="b"/>
    <xsl:param name="carry"/>
    <xsl:value-of select="format-number(($a + $b) mod 4294967296, '0')"/>
    <xsl:if test="$carry">
      <xsl:value-of select="concat(';', format-number(floor(($a + $b) div 4294967296), '0'), ';')"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="uint64-encode">
    <xsl:param name="word"/>
    <xsl:variable name="upper">
      <xsl:call-template name="uint32-encode">
        <xsl:with-param name="word" select="substring-before($word, ',,')"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="lower">
      <xsl:call-template name="uint32-encode">
        <xsl:with-param name="word" select="substring-after($word, ',,')"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="concat($lower, $upper)"/>
  </xsl:template>

  <xsl:template name="uint64-add-n">
    <xsl:param name="a"/>
    <xsl:param name="b"/>
    <xsl:variable name="lower">
      <xsl:call-template name="uint32-add">
        <xsl:with-param name="a" select="substring-after($a, ',,')"/>
        <xsl:with-param name="b" select="$b"/>
        <xsl:with-param name="carry" select="1"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="upper">
      <xsl:call-template name="uint32-add">
        <xsl:with-param name="a" select="substring-before($a, ',,')"/>
        <xsl:with-param name="b" select="substring-before(substring-after($lower, ';'), ';')"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="concat($upper, ',,', substring-before($lower, ';'))"/>
  </xsl:template>
</xsl:stylesheet>
