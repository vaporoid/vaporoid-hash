<?xml version="1.0" encoding="UTF-8"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements. See the NOTICE file distributed with this
  work for additional information regarding copyright ownership. The ASF
  licenses this file to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  License for the specific language governing permissions and limitations under
  the License.
-->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:vaporoid-hash="http://vaporoid.com/vaporoid-hash">
  <xsl:import href="vaporoid-hash.xsl"/>

  <xsl:output method="text"/>

  <xsl:variable name="endl">
    <xsl:text>
</xsl:text>
</xsl:variable>

  <xsl:template name="test-string-encode">
    <xsl:variable name="result">
      <xsl:call-template name="string-encode">
        <xsl:with-param name="string" select="'abc'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '616263'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-string-encode', $endl)"/>
  </xsl:template>

  <xsl:template name="test-string-add">
    <xsl:variable name="result">
      <xsl:call-template name="string-add">
        <xsl:with-param name="a" select="'foo'"/>
        <xsl:with-param name="b" select="'bar'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = 'foobar'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-string-add', $endl)"/>
  </xsl:template>

  <xsl:template name="test-string-fill">
    <xsl:variable name="result">
      <xsl:call-template name="string-fill">
        <xsl:with-param name="char" select="'0'"/>
        <xsl:with-param name="n" select="7"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '0000000'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-string-fill', $endl)"/>
  </xsl:template>

  <xsl:template name="test-uint32-decode">
    <xsl:variable name="result">
      <xsl:call-template name="uint32-decode">
        <!-- 0x01234567 = 19088743 -->
        <xsl:with-param name="byte" select="'67452301'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '19088743'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-uint32-decode', $endl)"/>
  </xsl:template>

  <xsl:template name="test-uint32-encode">
    <xsl:variable name="result">
      <xsl:call-template name="uint32-encode">
        <!-- 0x01234567 = 19088743 -->
        <xsl:with-param name="word" select="'19088743'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '67452301'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-uint32-encode', $endl)"/>
  </xsl:template>

  <xsl:template name="test-uint32-bitwise-and">
    <xsl:variable name="result">
      <xsl:call-template name="uint32-bitwise-and">
        <!-- 0x01234567 = 19088743 -->
        <xsl:with-param name="a" select="'19088743'"/>
        <!-- 0xfeedface = 4277009102 -->
        <xsl:with-param name="b" select="'4277009102'"/>
        <!-- 0x00214046 = 2179142 -->
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '2179142'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-uint32-bitwise-and', $endl)"/>
  </xsl:template>

  <xsl:template name="test-uint32-bitwise-xor">
    <xsl:variable name="result">
      <xsl:call-template name="uint32-bitwise-xor">
        <!-- 0x01234567 = 19088743 -->
        <xsl:with-param name="a" select="'19088743'"/>
        <!-- 0xfeedface = 4277009102 -->
        <xsl:with-param name="b" select="'4277009102'"/>
        <!-- 0xffcebfa9 = 4291739561 -->
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '4291739561'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-uint32-bitwise-xor', $endl)"/>
  </xsl:template>

  <xsl:template name="test-uint32-bitwise-or">
    <xsl:variable name="result">
      <xsl:call-template name="uint32-bitwise-or">
        <!-- 0x01234567 = 19088743 -->
        <xsl:with-param name="a" select="'19088743'"/>
        <!-- 0xfeedface = 4277009102 -->
        <xsl:with-param name="b" select="'4277009102'"/>
        <!-- 0xffefffef = 4293918703 -->
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '4293918703'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-uint32-bitwise-or', $endl)"/>
  </xsl:template>

  <xsl:template name="test-uint32-bitwise-not">
    <xsl:variable name="result">
      <xsl:call-template name="uint32-bitwise-and">
        <!-- 0x00a98ac7 = 11111111 -->
        <xsl:with-param name="a" select="'11111111'"/>
        <xsl:with-param name="b">
          <xsl:call-template name="uint32-bitwise-not">
            <!-- 0x05f5e0ff = 99999999 -->
            <xsl:with-param name="a" select="'99999999'"/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '526848'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-uint32-bitwise-not', $endl)"/>
  </xsl:template>

  <xsl:template name="test-uint32-left-shift-n">
    <xsl:variable name="result">
      <xsl:call-template name="uint32-encode">
        <xsl:with-param name="word">
          <xsl:call-template name="uint32-left-shift-n">
            <!-- 0x01234567 = 19088743 -->
            <xsl:with-param name="a" select="'19088743'"/>
            <xsl:with-param name="b" select="8"/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '00674523'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-uint32-left-shift-n', $endl)"/>
  </xsl:template>

  <xsl:template name="test-uint32-right-shift-n">
    <xsl:variable name="result">
      <xsl:call-template name="uint32-encode">
        <xsl:with-param name="word">
          <xsl:call-template name="uint32-right-shift-n">
            <!-- 0x01234567 = 19088743 -->
            <xsl:with-param name="a" select="'19088743'"/>
            <xsl:with-param name="b" select="8"/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '45230100'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-uint32-right-shift-n', $endl)"/>
  </xsl:template>

  <xsl:template name="test-uint32-add">
    <xsl:variable name="result">
      <xsl:call-template name="uint32-add">
        <!-- 0x01234567 = 19088743 -->
        <xsl:with-param name="a" select="'19088743'"/>
        <!-- 0xfeedface = 4277009102 -->
        <xsl:with-param name="b" select="'4277009102'"/>
        <xsl:with-param name="carry" select="1"/>
        <!-- 0x100114035 = 4296097845 -->
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '1130549;1;'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-uint32-add', $endl)"/>
  </xsl:template>

  <xsl:template name="test-uint32-add-2">
    <xsl:variable name="result">
      <xsl:call-template name="uint32-add">
        <!-- 0xffffffff = 4294967295 -->
        <xsl:with-param name="a" select="'4294967295'"/>
        <xsl:with-param name="b" select="'1'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '0'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-uint32-add-2', $endl)"/>
  </xsl:template>

  <xsl:template name="test-uint64-encode">
    <xsl:variable name="result">
      <xsl:call-template name="uint64-encode">
        <!-- 0xfeedface01234567 -->
        <xsl:with-param name="word" select="'4277009102,,19088743'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '67452301cefaedfe'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-uint64-encode', $endl)"/>
  </xsl:template>

  <xsl:template name="test-uint64-add-n">
    <xsl:variable name="result">
      <xsl:call-template name="uint64-add-n">
        <!-- 0xfffffffffffffff0 -->
        <xsl:with-param name="a" select="'4294967295,,4294967280'"/>
        <xsl:with-param name="b" select="256"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '0,,240'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-uint64-add-n', $endl)"/>
  </xsl:template>

  <xsl:template name="test-md5-transform-1">
    <xsl:variable name="result">
      <xsl:call-template name="md5-encode">
        <xsl:with-param name="state">
          <xsl:call-template name="md5-transform">
            <xsl:with-param name="state">
              <xsl:call-template name="md5-init"/>
            </xsl:with-param>
            <xsl:with-param name="buffer">
              <xsl:text>80000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000</xsl:text>
            </xsl:with-param>
            <xsl:with-param name="offset" select="0"/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = 'd41d8cd98f00b204e9800998ecf8427e'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-md5-transform-1', $endl)"/>
  </xsl:template>

  <xsl:template name="test-md5-transform-2">
    <xsl:variable name="result">
      <xsl:call-template name="md5-encode">
        <xsl:with-param name="state">
          <xsl:call-template name="md5-transform">
            <xsl:with-param name="state">
              <xsl:call-template name="md5-init"/>
            </xsl:with-param>
            <xsl:with-param name="buffer">
              <xsl:text>61626380000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001800000000000000</xsl:text>
            </xsl:with-param>
            <xsl:with-param name="offset" select="0"/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '900150983cd24fb0d6963f7d28e17f72'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-md5-transform-2', $endl)"/>
  </xsl:template>

  <xsl:template name="test-md5-byte-1">
    <xsl:variable name="result">
      <xsl:call-template name="vaporoid-hash:md5">
        <xsl:with-param name="byte"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = 'd41d8cd98f00b204e9800998ecf8427e'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-md5-byte-1', $endl)"/>
  </xsl:template>

  <xsl:template name="test-md5-byte-2">
    <xsl:variable name="result">
      <xsl:call-template name="vaporoid-hash:md5">
        <xsl:with-param name="byte">
          <xsl:text>616263</xsl:text>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '900150983cd24fb0d6963f7d28e17f72'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-md5-byte-2', $endl)"/>
  </xsl:template>

  <xsl:template name="test-md5-string-1">
    <xsl:variable name="result">
      <xsl:call-template name="vaporoid-hash:md5">
        <xsl:with-param name="string"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = 'd41d8cd98f00b204e9800998ecf8427e'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-md5-string-1', $endl)"/>
  </xsl:template>

  <xsl:template name="test-md5-string-2">
    <xsl:variable name="result">
      <xsl:call-template name="vaporoid-hash:md5">
        <xsl:with-param name="string">
          <xsl:text>abc</xsl:text>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = '900150983cd24fb0d6963f7d28e17f72'">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test-md5-string-2', $endl)"/>
  </xsl:template>

  <xsl:template match="/">
    <xsl:call-template name="test-string-encode"/>
    <xsl:call-template name="test-string-add"/>
    <xsl:call-template name="test-string-fill"/>
    <xsl:call-template name="test-uint32-decode"/>
    <xsl:call-template name="test-uint32-encode"/>
    <xsl:call-template name="test-uint32-bitwise-and"/>
    <xsl:call-template name="test-uint32-bitwise-xor"/>
    <xsl:call-template name="test-uint32-bitwise-or"/>
    <xsl:call-template name="test-uint32-bitwise-not"/>
    <xsl:call-template name="test-uint32-left-shift-n"/>
    <xsl:call-template name="test-uint32-right-shift-n"/>
    <xsl:call-template name="test-uint32-add"/>
    <xsl:call-template name="test-uint64-encode"/>
    <xsl:call-template name="test-uint32-add-2"/>
    <xsl:call-template name="test-uint64-add-n"/>
    <xsl:call-template name="test-md5-transform-1"/>
    <xsl:call-template name="test-md5-transform-2"/>
    <xsl:call-template name="test-md5-byte-1"/>
    <xsl:call-template name="test-md5-byte-2"/>
    <xsl:call-template name="test-md5-string-1"/>
    <xsl:call-template name="test-md5-string-2"/>
    <xsl:apply-templates select="//test"/>
  </xsl:template>

  <xsl:template match="test">
    <xsl:variable name="result">
      <xsl:call-template name="vaporoid-hash:md5">
        <xsl:with-param name="string" select="source"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$result = target">
        <xsl:text>[PASS]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[FAIL]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="concat(' test@name = ', @name, $endl)"/>
  </xsl:template>
</xsl:stylesheet>
