<?xml version="1.0" encoding="UTF-8"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements. See the NOTICE file distributed with this
  work for additional information regarding copyright ownership. The ASF
  licenses this file to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  License for the specific language governing permissions and limitations under
  the License.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="UTF-8"/>

  <xsl:template match="source">
    <xsl:copy-of select="document(concat(current(), '.xsl'))//xsl:template"/>
  </xsl:template>

  <xsl:template match="/">
    <xsl:comment>
      <xsl:text>
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with this
  work for additional information regarding copyright ownership.  The ASF
  licenses this file to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
  License for the specific language governing permissions and limitations under
  the License.
</xsl:text>
    </xsl:comment>
    <xsl:element name="xsl:stylesheet">
      <xsl:attribute name="version">
        <xsl:text>1.0</xsl:text>
      </xsl:attribute>

      <xsl:element name="xsl:key">
        <xsl:attribute name="name">
          <xsl:text>vaporoid-hash-table</xsl:text>
        </xsl:attribute>
        <xsl:attribute name="match">
          <xsl:text>item</xsl:text>
        </xsl:attribute>
        <xsl:attribute name="use">
          <xsl:text>@key</xsl:text>
        </xsl:attribute>
      </xsl:element>

      <xsl:element name="xsl:variable">
        <xsl:attribute name="name">
          <xsl:text>endl</xsl:text>
        </xsl:attribute>
        <xsl:element name="xsl:text">
          <xsl:text>
</xsl:text>
        </xsl:element>
      </xsl:element>

      <xsl:apply-templates match="source"/>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
