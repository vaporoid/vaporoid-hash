vaporoid-hash 0.2
================================================================================


これはなに？
--------------------------------------------------------------------------------

XSLT 1.0だけを用いたメッセージダイジェストの実装です。現在はMD5だけが利用可能で
すが、将来的にSHA-1やSHA-2も利用可能になるかもしれません。

現在のバージョンのMD5は、小さなメッセージの計算には（かろうじて）利用可能でしょ
う。速度の問題から、大きなメッセージの計算は実質的に不可能です。


注意！　注意！　注意！
--------------------------------------------------------------------------------

いくつか効率化しましたが、まだまだまだまだ、すごくとてもめちゃめちゃ遅いです。
64KBのメッセージダイジェストを計算するためにかかった時間を以下に示します。

* OpenSSL: 0.07秒
* vaporoid-hash: 51秒

うひゃあ！　これはxsltproc（つまり、libxml2とlibxslt）での結果なので、処理系に
よって速度は大きく変わるでしょう（FireFoxのTransformiixではもっと早く、IEの
MSXMLではもっと遅くなりました）。


使い方
--------------------------------------------------------------------------------

現在のの実装は、vaporoid-hash.xslという単一のファイルと、テーブルとして使うXML
ファイルで構成されています。

* vaporoid-hash.xsl
* vaporoid-hash-char-encode-1.xml
* vaporoid-hash-char-encode-2.xml
* vaporoid-hash-char-encode-3.xml
* vaporoid-hash-uint4-bitwise-and.xml
* vaporoid-hash-uint4-bitwise-or.xml
* vaporoid-hash-uint4-bitwise-xor.xml
* vaporoid-hash-uint8-decode.xml

単純な例を示します。

    <?xml version="1.0" encoding="UTF-8"?>
    <xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:vaporoid-hash="http://vapororoid.com/vaporoid-hash">
      <xsl:import href="vaporoid-hash.xsl"/>
      <xsl:output method="text"/>
      <xsl:template match="/">
        <xsl:call-template name="vaporoid-hash:md5">
          <xsl:with-param name="string" select="'abc'"/>
        </xsl:call-template>
        <xsl:text>
    </xsl:text>
      </xsl:template>
    </xsl:stylesheet>

この例は、文字列abcを（stringという名前で）与えてmd5テンプレートを呼び出してい
ます。md5テンプレートは、文字列abcをUTF-8としてバイト列に変換してから、メッセー
ジダイジェストを計算します。

また、デモ用のHTMLが用意してあります。運が良ければ、あなたのブラウザでこれが動
くでしょう。


テンプレートシンタックス
--------------------------------------------------------------------------------

stringをバイト列に変換してから、メッセージダイジェストを計算します。

    <xsl:call-template name="vaporoid-hash:md5">
      <xsl:with-param name="string" select="string"/>
    </xsl:call-template>

byteを16進数表記されたバイト列とみなして、メッセージダイジェストを計算します。

    <xsl:call-template name="vaporoid-hash:md5">
      <xsl:with-param name="byte" select="string"/>
    </xsl:call-template>


実装について
--------------------------------------------------------------------------------

check.xmlをブラウザで開くと、テストが実行できます。

Makefileが用意してあります。

    $ make
    $ make check
    $ make bench

以下のツールが必要になるかもしれません。

* xmllint ([libxml2](http://www.xmlsoft.org/))
* xsltproc ([libxslt](http://xmlsoft.org/XSLT/))
* [OpenSSL](http://www.openssl.org/)
* [Ragel State Machine Compiler](http://www.complang.org/ragel/)
* [The LEMON Parser Generator](http://www.hwaci.com/sw/lemon/)


文字列からバイト列への変換について
--------------------------------------------------------------------------------

テーブルを使って一文字ずつ変換しています。UTF-8で表現したら何バイトになるかによ
って複数のファイルに分割されており、動的に必要なぶんだけロードされます。つまり、
ASCIIと互換なエリア（UTF-8で表現したら1バイトになるエリア）の文字しか使ってい
なければ、メモリ効率は大幅に改善されます。


符号無し整数の演算の実装について
--------------------------------------------------------------------------------

numberをuint32として扱い、ビット演算はテーブルで実装しています。


MD5の実装について
--------------------------------------------------------------------------------

RFC 1321の内容をDSLで記述し、XSLに変換しています。変換用のコンパイラの記述に
ragelとlemonを使っています。

* 現状はコンパイラというより逐語的なXSLへの置換なので、最適化の余地おおいにあり！
* コンパイラがメモリリークするかもしれないけど気にしない！


今後の計画
--------------------------------------------------------------------------------

* 高速化する。
* SHA-1やSHA-2に対応する。

