#include <stdint.h>
#include <sstream>
#include <stdexcept>
#include <string>
#include <map>

class token {
public:
  explicit token(int line)
    : number_(), line_(line) {}

  explicit token(const std::string& string, int line)
    : string_(string), number_(), line_(line) {}

  explicit token(uintmax_t number, int line)
    : number_(number), line_(line) {}

  const std::string& string() const {
    return string_;
  }

  uintmax_t number() const {
    return number_;
  }

  int line() const {
    return line_;
  }

private:
  std::string string_;
  uintmax_t number_;
  int line_;

  token(const token&);
  token& operator=(const token&);
};

class context {
public:
  explicit context() {}

  void set_constant(const std::string& key, uintmax_t value) {
    constant_[key] = value;
  }

  uintmax_t get_constant(const std::string& key) const {
    std::map<std::string, uintmax_t>::const_iterator i = constant_.find(key);
    if (i == constant_.end()) {
      std::ostringstream out;
      out << "Not found: key = " << key;
      throw std::runtime_error(out.str());
    }
    return i->second;
  }

  void clear_variable() {
    variable_.clear();
    variable_count_ = 0;
  }

  std::string assign_variable(const std::string& key) {
    variable_[key] = variable_count_++;
    std::ostringstream out;
    out << "_" << variable_[key];
    return out.str();
  }

  std::string get_variable(const std::string& key) const {
    std::map<std::string, int>::const_iterator i = variable_.find(key);
    if (i == variable_.end()) {
      std::ostringstream out;
      out << "Not found: key = " << key;
      throw std::runtime_error(out.str());
    }
    std::ostringstream out;
    out << "_" << i->second;
    return out.str();
  }

private:
  std::map<std::string, uintmax_t> constant_;
  std::map<std::string, int> variable_;
  int variable_count_;

  context(const context&);
  context& operator=(const context&);
};

inline std::string* new_text() {
  return new std::string();
}

inline std::string* new_text(const std::string& string) {
  return new std::string(string);
}

inline void delete_text(std::string* text) {
  delete text;
}
