// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements. See the NOTICE file distributed with this
// work for additional information regarding copyright ownership. The ASF
// licenses this file to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

#include <stdint.h>
#include <iomanip>
#include <iostream>

inline uint32_t bitwise_and(uint32_t a, uint32_t b) {
  return a & b;
}

inline uint32_t bitwise_xor(uint32_t a, uint32_t b) {
  return a ^ b;
}

inline uint32_t bitwise_or(uint32_t a, uint32_t b) {
  return a | b;
}

template <typename T>
inline void print_uint4_bitwise(std::ostream& out, const char* name, T fn) {
  out << "\n"
      << "  <xsl:template name=\"uint4-" << name << "\">\n"
      << "    <xsl:param name=\"a\"/>\n"
      << "    <xsl:param name=\"b\"/>\n"
      << "    <xsl:for-each select=\"document('vaporoid-hash-uint4-" << name << ".xml')\">\n"
      << "      <xsl:value-of select=\"key('vaporoid-hash-table', concat($a, 'x' ,$b))\"/>\n"
      << "    </xsl:for-each>\n"
      << "  </xsl:template>\n";
}


inline void print_uintN_bitwise(std::ostream& out, uint32_t n, const char* name) {
  uint32_t m = n / 2;
  uint32_t x = 1 << m;
  out << "\n"
      << "  <xsl:template name=\"uint" << n << "-" << name << "\">\n"
      << "    <xsl:param name=\"a\"/>\n"
      << "    <xsl:param name=\"b\"/>\n"
      << "    <xsl:variable name=\"upper\">\n"
      << "      <xsl:call-template name=\"uint" << m << "-" << name << "\">\n"
      << "        <xsl:with-param name=\"a\" select=\"floor($a div " << x << ")\"/>\n"
      << "        <xsl:with-param name=\"b\" select=\"floor($b div " << x << ")\"/>\n"
      << "      </xsl:call-template>\n"
      << "    </xsl:variable>\n"
      << "    <xsl:variable name=\"lower\">\n"
      << "      <xsl:call-template name=\"uint" << m << "-" << name << "\">\n"
      << "        <xsl:with-param name=\"a\" select=\"$a mod " << x << "\"/>\n"
      << "        <xsl:with-param name=\"b\" select=\"$b mod " << x << "\"/>\n"
      << "      </xsl:call-template>\n"
      << "    </xsl:variable>\n"
      << "    <xsl:value-of select=\"$upper * " << x << " + $lower\"/>\n"
      << "  </xsl:template>\n";
}

inline void print_uint8_decode(std::ostream& out) {
  out << "\n"
      << "  <xsl:template name=\"uint8-decode\">\n"
      << "    <xsl:param name=\"byte\"/>\n"
      << "    <xsl:for-each select=\"document('vaporoid-hash-uint8-decode.xml')\">\n"
      << "      <xsl:value-of select=\"key('vaporoid-hash-table', $byte)\"/>\n"
      << "    </xsl:for-each>\n"
      << "  </xsl:template>\n";
}

inline void print_uint8_encode(std::ostream& out) {
  out << "\n"
      << "  <xsl:template name=\"uint8-encode\">\n"
      << "    <xsl:param name=\"word\"/>\n"
      << "    <xsl:choose>\n";
  for (uint32_t i = 0; i < 256; ++i) {
    out << "      <xsl:when test=\"$word = " << i << "\">\n"
        << "        <xsl:text>" << std::hex << std::setfill('0') << std::setw(2) << i << std::dec << std::setfill(' ') << "</xsl:text>\n"
        << "      </xsl:when>\n";
  }
  out << "      <xsl:otherwise>\n"
      << "        <xsl:message>\n"
      << "          <xsl:value-of select=\"concat('Range error: $word = ', $word, ' (uint8-encode)', $endl)\"/>\n"
      << "        </xsl:message>\n"
      << "      </xsl:otherwise>\n"
      << "    </xsl:choose>\n"
      << "  </xsl:template>\n";
}

inline void print_uint32_bitwise(std::ostream& out, const char* name) {
  out << "\n"
      << "  <xsl:template name=\"uint32-" << name << "\">\n"
      << "    <xsl:param name=\"a\"/>\n"
      << "    <xsl:param name=\"b\"/>\n"
      << "    <xsl:variable name=\"upper\">\n"
      << "      <xsl:call-template name=\"uint16-" << name << "\">\n"
      << "        <xsl:with-param name=\"a\" select=\"floor($a div 65536)\"/>\n"
      << "        <xsl:with-param name=\"b\" select=\"floor($b div 65536)\"/>\n"
      << "      </xsl:call-template>\n"
      << "    </xsl:variable>\n"
      << "    <xsl:variable name=\"lower\">\n"
      << "      <xsl:call-template name=\"uint16-" << name << "\">\n"
      << "        <xsl:with-param name=\"a\" select=\"$a mod 65536\"/>\n"
      << "        <xsl:with-param name=\"b\" select=\"$b mod 65536\"/>\n"
      << "      </xsl:call-template>\n"
      << "    </xsl:variable>\n"
      << "    <xsl:value-of select=\"format-number($upper * 65536 + $lower, '0')\"/>\n"
      << "  </xsl:template>\n";
}

inline void print(std::ostream& out) {
  out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      << "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
      << "  <xsl:key name=\"vaporoid-hash-table\" match=\"vaporoid-hash-item\" use=\"@key\"/>\n"
      << "  <xsl:variable name=\"endl\">\n"
      << "    <xsl:text>\n"
      << "</xsl:text>\n"
      << "  </xsl:variable>\n";
  print_uint4_bitwise(out, "bitwise-and", bitwise_and);
  print_uint4_bitwise(out, "bitwise-xor", bitwise_xor);
  print_uint4_bitwise(out, "bitwise-or", bitwise_or);
  print_uintN_bitwise(out, 8, "bitwise-and");
  print_uintN_bitwise(out, 8, "bitwise-xor");
  print_uintN_bitwise(out, 8, "bitwise-or");
  print_uint8_decode(out);
  print_uint8_encode(out);
  print_uintN_bitwise(out, 16, "bitwise-and");
  print_uintN_bitwise(out, 16, "bitwise-xor");
  print_uintN_bitwise(out, 16, "bitwise-or");
  print_uint32_bitwise(out, "bitwise-and");
  print_uint32_bitwise(out, "bitwise-xor");
  print_uint32_bitwise(out, "bitwise-or");
  out << "</xsl:stylesheet>\n";
}

int main(int, char* []) {
  print(std::cout);
  return 0;
}
