// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements. See the NOTICE file distributed with this
// work for additional information regarding copyright ownership. The ASF
// licenses this file to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

#include <stdint.h>
#include <iomanip>
#include <iostream>
#include <string>

inline uint8_t* encode_utf8(uint32_t source, uint8_t* target) {
  if (source <= 0x00007f) {
    *target++ = source;
  } else if (source <= 0x0007ff) {
    *target++ = 0xc0 | (source >> 6);
    *target++ = 0x80 | (source & 0x3f);
  }
  else if (source <= 0x00ffff) {
    *target++ = 0xe0 | (source >> 12);
    *target++ = 0x80 | (source >> 6 & 0x3f);
    *target++ = 0x80 | (source & 0x3f);
  } else {
    *target++ = 0xf0 | (source >> 18);
    *target++ = 0x80 | (source >> 12 & 0x3f);
    *target++ = 0x80 | (source >> 6 & 0x3f);
    *target++ = 0x80 | (source & 0x3f);
  }
  return target;
}

inline void print_char_encode_item(std::ostream& out, uint32_t c) {
  uint8_t byte[4] = { 0 };
  const uint8_t* end = encode_utf8(c, byte);

  out << "  <item key=\"" << "&#x" << std::hex << c << std::dec << ";\">";
  for (const uint8_t* i = byte; i != end; ++i) {
    out << std::hex << std::setfill('0') << std::setw(2) << static_cast<uint16_t>(*i) << std::dec << std::setfill(' ');
  }
  out << "</item>\n";
}

inline void print_char_encode(std::ostream& out, uint32_t begin, uint32_t end) {
  static const uint32_t range[] = {
    0x000009, 0x000009,
    0x00000a, 0x00000a,
    0x00000d, 0x00000d,
    0x000020, 0x00d7ff,
    0x00e000, 0x00fffd,
    0x010000, 0x10ffff,
    0,
  };

  out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      << "<table>\n";
  for (size_t i = 0; range[i] != 0; i += 2) {
    for (uint32_t c = range[i]; c <= range[i + 1]; ++c) {
      if (begin <= c && c < end) {
        print_char_encode_item(out, c);
      }
    }
  }
  out << "</table>\n";
}

inline uint32_t bitwise_and(uint32_t a, uint32_t b) {
  return a & b;
}

inline uint32_t bitwise_xor(uint32_t a, uint32_t b) {
  return a ^ b;
}

inline uint32_t bitwise_or(uint32_t a, uint32_t b) {
  return a | b;
}

template <typename T>
inline void print_uintN_bitwise(std::ostream& out, uint32_t n, T fn) {
  out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      << "<table>\n";
  for (uint32_t a = 0; a < n; ++a) {
    for (uint32_t b = 0; b < n; ++b) {
      out << "<item key=\"" << a << "x" << b << "\">" << fn(a, b) << "</item>\n";
    }
  }
  out << "</table>\n";
}

inline void print_uintN_decode(std::ostream& out, uint32_t n) {
  out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      << "<table>\n";
  for (uint32_t i = 0; i < n; ++i) {
    out << "<item key=\"" << std::hex << std::setfill('0') << std::setw(2) << i << std::dec << std::setfill(' ') << "\">" << i << "</item>\n";
  }
  out << "</table>\n";
}

int main(int argc, char* argv[]) {
  std::string name = argv[1];
  if (name == "char-encode-1") {
    print_char_encode(std::cout, 0x000000, 0x000080);
  } else if (name == "char-encode-2") {
    print_char_encode(std::cout, 0x000080, 0x000800);
  } else if (name == "char-encode-3") {
    print_char_encode(std::cout, 0x000800, 0x010000);
  } else if (name == "char-encode-4") {
    print_char_encode(std::cout, 0x010000, 0x110000);
  } else if (name == "uint4-bitwise-and") {
    print_uintN_bitwise(std::cout, 1 << 4, bitwise_and);
  } else if (name == "uint4-bitwise-xor") {
    print_uintN_bitwise(std::cout, 1 << 4, bitwise_xor);
  } else if (name == "uint4-bitwise-or") {
    print_uintN_bitwise(std::cout, 1 << 4, bitwise_or);
  } else if (name == "uint8-decode") {
    print_uintN_decode(std::cout, 1 << 8);
  } else if (name == "uint16-decode") {
    print_uintN_decode(std::cout, 1 << 16);
  }
  return 0;
}
