RM          = rm -f
CPPFLAGS    = -I.
CC          = g++
CCFLAGS     = -Wall -Wextra -O3
CXX         = g++
CXXFLAGS    = -Wall -Wextra -O3
LDFLAGS     =
TARGET_ARCH =
LOADLIBES   =
LDLIBS      =

COMPILE.c = $(CC) $(CCFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
COMPILE.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
COMPILE.cxx = $(COMPILE.cc)

LINK.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)
LINK.cpp = $(LINK.cc)

TARGET = $(TARGET_EXE) $(TARGET_XSL) $(TARGET_XML)

TARGET_EXE = \
	generate \
	generate-uint_ \
	generate-table

TARGET_XSL = \
	uint_.xsl \
	md5_.xsl \
	vaporoid-hash.xsl

TARGET_XML = \
	vaporoid-hash-char-encode-1.xml \
	vaporoid-hash-char-encode-2.xml \
	vaporoid-hash-char-encode-3.xml \
	vaporoid-hash-uint4-bitwise-and.xml \
	vaporoid-hash-uint4-bitwise-xor.xml \
	vaporoid-hash-uint4-bitwise-or.xml \
	vaporoid-hash-uint8-decode.xml

SUBDIR = exp

all: $(TARGET)
	for i in $(SUBDIR); do \
		(cd $$i && $(MAKE) all); \
	done

clean:
	$(RM) $(TARGET) *.h *.c *.cxx *.o *.out
	for i in $(SUBDIR); do \
		(cd $$i && $(MAKE) clean); \
	done

release:
	$(RM) $(TARGET_EXE) *.o *.out
	for i in $(SUBDIR); do \
		(cd $$i && $(MAKE) release); \
	done

bench: $(TARGET)
	xsltproc bench-data.xsl bench.xml | time openssl md5
	time xsltproc --timing bench.xsl bench.xml

check: $(TARGET)
	xsltproc test.xsl test.xml

generate: parser.o generate.o
	$(LINK.cpp) parser.o generate.o $(LOADLIBES) $(LDLIBS) -o $@

uint_.xsl: generate-uint_
	./generate-uint_ >$@

md5_.xsl: generate md5_.dat
	./generate md5_.dat >md5_.xsl
	xmllint --format md5_.xsl >md5_.xsl.new
	mv -f md5_.xsl.new md5_.xsl

vaporoid-hash.xsl: merge.xsl merge.xml
	xsltproc merge.xsl merge.xml >vaporoid-hash.xsl
	xmllint --format vaporoid-hash.xsl >vaporoid-hash.xsl.new
	mv -f vaporoid-hash.xsl.new vaporoid-hash.xsl

vaporoid-hash-char-encode-1.xml: generate-table
	./generate-table char-encode-1 >$@

vaporoid-hash-char-encode-2.xml: generate-table
	./generate-table char-encode-2 >$@

vaporoid-hash-char-encode-3.xml: generate-table
	./generate-table char-encode-3 >$@

vaporoid-hash-uint4-bitwise-and.xml: generate-table
	./generate-table uint4-bitwise-and >$@

vaporoid-hash-uint4-bitwise-xor.xml: generate-table
	./generate-table uint4-bitwise-xor >$@

vaporoid-hash-uint4-bitwise-or.xml: generate-table
	./generate-table uint4-bitwise-or >$@

vaporoid-hash-uint8-decode.xml: generate-table
	./generate-table uint8-decode >$@

.SUFFIXES: .cpp .y .c .rl .cxx .o
.PRECIOUS: %.c %.cxx

.cpp:
	$(LINK.cpp) $< $(LOADLIBES) $(LDLIBS) -o $@

.y.c:
	lemon $<

.c.o:
	$(COMPILE.c) $< -o $@

.rl.cxx:
	ragel $< -o $@

.cxx.o:
	$(COMPILE.cxx) $< -o $@
