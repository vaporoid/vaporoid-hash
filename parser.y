// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements. See the NOTICE file distributed with this
// work for additional information regarding copyright ownership. The ASF
// licenses this file to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

%name parser

%include {
  #include <assert.h>
  #include <iostream>
  #include <sstream>
  #include <stdexcept>
  #include <string>
  #include "parser.hpp"
}

%token_prefix TK_
%token_type {token*}
%token_destructor { delete $$; }
%extra_argument { context* ctx }

%syntax_error {
  std::ostringstream out;
  out << "Syntax error: token = " << yyTokenName[yymajor] << ", line = " << TOKEN->line();
  throw std::runtime_error(out.str());
}

%type expression_list { std::string* }
%type expression      { std::string* }
%type pack_params     { std::string* }
%type param_list      { std::string* }
%type params          { std::string* }
%type with_param_list { std::string* }
%type with_params     { std::string* }

main ::= expression_list(X). {
  std::cout
      << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      << "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
      << *X
      << "  <xsl:variable name=\"endl\">\n"
      << "    <xsl:text>\n"
      << "</xsl:text>\n"
      << "  </xsl:variable>\n"
      << "</xsl:stylesheet>\n";
  delete_text(X);
}

expression_list(A) ::= . {
  A = new_text();
}

expression_list(A) ::= expression_list(X) expression(Y). {
  A = new_text(*X + *Y);
  delete_text(X);
  delete_text(Y);
}

expression(A) ::= MACRO_GET LEFT_SQUARE_BRACKET IDENTIFIER(X) RIGHT_SQUARE_BRACKET. {
  std::ostringstream out;
  out << "<xsl:text>" << ctx->get_constant(X->string()) << "</xsl:text>\n";
  A = new_text(out.str());
}

expression(A) ::= MACRO_SET LEFT_SQUARE_BRACKET IDENTIFIER(X) COMMA NUMBER(Y) RIGHT_SQUARE_BRACKET. {
  ctx->set_constant(X->string(), Y->number());
  A = new_text();
}

expression(A) ::= MACRO_IMPORT LEFT_SQUARE_BRACKET IDENTIFIER(X) RIGHT_SQUARE_BRACKET. {
  std::ostringstream out;
  out << "<xsl:import href=\"" << X->string() << ".xsl\"/>\n";
  A = new_text(out.str());
}

expression(A) ::= MACRO_EXPRESSION LEFT_SQUARE_BRACKET EXPRESSION(X) RIGHT_SQUARE_BRACKET. {
  std::ostringstream out;
  out << "<xsl:value-of select=\"" << X->string() << "\"/>\n";
  A = new_text(out.str());
}

expression(A) ::= MACRO_DEBUG LEFT_SQUARE_BRACKET expression(X) RIGHT_SQUARE_BRACKET. {
  std::ostringstream out;
  out << "<xsl:message>\n"
      << "<xsl:text>[DEBUG] </xsl:text>"
      << *X
      << "<xsl:value-of select=\"$endl\"/>\n"
      << "</xsl:message>\n";
  A = new_text(out.str());
  delete_text(X);
}

pack_params(A) ::= expression(X). {
  std::ostringstream out;
  out << *X
      << "<xsl:text>;</xsl:text>\n";
  A = new_text(out.str());
  delete_text(X);
}

pack_params(A) ::= pack_params(X) COMMA expression(Y). {
  std::ostringstream out;
  out << *X
      << *Y
      << "<xsl:text>;</xsl:text>\n";
  A = new_text(out.str());
  delete_text(X);
  delete_text(Y);
}

expression(A) ::= MACRO_PACK LEFT_SQUARE_BRACKET pack_params(X) RIGHT_SQUARE_BRACKET. {
  A = X;
}

expression(A) ::= MACRO_UNPACK LEFT_SQUARE_BRACKET VARIABLE(X) COMMA NUMBER(Y) RIGHT_SQUARE_BRACKET. {
  std::ostringstream out;
  out << "<xsl:value-of select=\"substring-before(";
  for (uintmax_t i = 0; i < Y->number(); ++i) {
    out << "substring-after(";
  }
  out << "$" << X->string();
  for (uintmax_t i = 0; i < Y->number(); ++i) {
    out << ", ';')";
  }
  out << ", ';')\"/>\n";
  A = new_text(out.str());
}

expression(A) ::= MACRO_UNPACK LEFT_SQUARE_BRACKET REGISTER(X) COMMA NUMBER(Y) RIGHT_SQUARE_BRACKET. {
  std::ostringstream out;
  out << "<xsl:value-of select=\"substring-before(";
  for (uintmax_t i = 0; i < Y->number(); ++i) {
    out << "substring-after(";
  }
  out << "$" << ctx->get_variable(X->string());
  for (uintmax_t i = 0; i < Y->number(); ++i) {
    out << ", ';')";
  }
  out << ", ';')\"/>\n";
  A = new_text(out.str());
}

expression(A) ::= MACRO_UINT32 LEFT_SQUARE_BRACKET NUMBER(X) RIGHT_SQUARE_BRACKET. {
  std::ostringstream out;
  out << "<xsl:text>"
      << X->number()
      << "</xsl:text>\n";
  A = new_text(out.str());
}

expression(A) ::= MACRO_UINT64 LEFT_SQUARE_BRACKET NUMBER(X) RIGHT_SQUARE_BRACKET. {
  std::ostringstream out;
  out << "<xsl:text>"
      << ((X->number() >> 32) & 0xffffffff)
      << ",,"
      << (X->number() & 0xffffffff)
      << "</xsl:text>\n";
  A = new_text(out.str());
}

param_list(A) ::= LEFT_PARENTHESIS RIGHT_PARENTHESIS. {
  A = new_text();
}

param_list(A) ::= LEFT_PARENTHESIS params(X) RIGHT_PARENTHESIS. {
  A = X;
}

params(A) ::= IDENTIFIER(X). {
  std::ostringstream out;
  out << "<xsl:param name=\"" << X->string() << "\"/>\n";
  A = new_text(out.str());
}

params(A) ::= params(X) COMMA IDENTIFIER(Y). {
  std::ostringstream out;
  out << *X
      << "<xsl:param name=\"" << Y->string() << "\"/>\n";
  A = new_text(out.str());
  delete_text(X);
}

expression(A) ::= TEMPLATE IDENTIFIER(X) param_list(Y) expression_list(Z) END. {
  std::ostringstream out;
  out << "<xsl:template name=\"" << X->string() << "\">\n"
      << *Y
      << *Z
      << "</xsl:template>\n";
  A = new_text(out.str());
  delete_text(Y);
  delete_text(Z);
  ctx->clear_variable();
}

expression(A) ::= IF REGISTER(X) THEN expression_list(Y) END. {
  std::ostringstream out;
  out << "<xsl:if test=\"$" << ctx->get_variable(X->string()) << " != 0\">\n"
      << *Y
      << "</xsl:if>\n";
  A = new_text(out.str());
  delete_text(Y);
}

expression(A) ::= IF REGISTER(X) THEN expression_list(Y) ELSE expression_list(Z) END. {
  std::ostringstream out;
  out << "<xsl:choose>\n"
      << "<xsl:when test=\"$" << ctx->get_variable(X->string()) << " != 0\">\n"
      << *Y
      << "</xsl:when>\n"
      << "<xsl:otherwise>\n"
      << *Z
      << "</xsl:otherwise>\n"
      << "</xsl:choose>\n";
  A = new_text(out.str());
  delete_text(Y);
  delete_text(Z);
}

with_param_list(A) ::= LEFT_PARENTHESIS RIGHT_PARENTHESIS. {
  A = new_text();
}

with_param_list(A) ::= LEFT_PARENTHESIS with_params(X) RIGHT_PARENTHESIS. {
  A = X;
}

with_params(A) ::= IDENTIFIER(X) WITH expression(Y). {
  std::ostringstream out;
  out << "<xsl:with-param name=\"" << X->string() << "\">\n"
      << *Y
      << "</xsl:with-param>\n";
  A = new_text(out.str());
  delete_text(Y);
}

with_params(A) ::= with_params(X) COMMA IDENTIFIER(Y) WITH expression(Z). {
  std::ostringstream out;
  out << *X
      << "<xsl:with-param name=\"" << Y->string() << "\">\n"
      << *Z
      << "</xsl:with-param>\n";
  A = new_text(out.str());
  delete_text(X);
  delete_text(Z);
}

expression(A) ::= IDENTIFIER(X) with_param_list(Y). {
  std::ostringstream out;
  out << "<xsl:call-template name=\"" << X->string() << "\">\n"
      << *Y
      << "</xsl:call-template>\n";
  A = new_text(out.str());
  delete_text(Y);
}

expression(A) ::= VARIABLE(X). {
  std::ostringstream out;
  out << "<xsl:value-of select=\"$" << X->string() << "\"/>\n";
  A = new_text(out.str());
}

expression(A) ::= VARIABLE(X) EQUALS_SIGN expression(Y). {
  std::ostringstream out;
  out << "<xsl:variable name=\"" << X->string() << "\">\n"
      << *Y
      << "</xsl:variable>\n";
  A = new_text(out.str());
  delete_text(Y);
}

expression(A) ::= REGISTER(X). {
  std::ostringstream out;
  out << "<xsl:value-of select=\"$" << ctx->get_variable(X->string()) << "\"/>\n";
  A = new_text(out.str());
}

expression(A) ::= REGISTER(X) EQUALS_SIGN expression(Y). {
  std::ostringstream out;
  out << "<xsl:variable name=\"" << ctx->assign_variable(X->string()) << "\">\n"
      << *Y
      << "</xsl:variable>\n";
  A = new_text(out.str());
  delete_text(Y);
}
