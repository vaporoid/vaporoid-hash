<?xml version="1.0" encoding="UTF-8"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements. See the NOTICE file distributed with this
  work for additional information regarding copyright ownership. The ASF
  licenses this file to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  License for the specific language governing permissions and limitations under
  the License.
-->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:vaporoid-hash="http://vaporoid.com/vaporoid-hash">
  <xsl:import href="md5_.xsl"/>

  <xsl:template name="vaporoid-hash:md5">
    <xsl:param name="string"/>
    <xsl:param name="byte"/>

    <xsl:choose>
      <xsl:when test="$string">
        <xsl:call-template name="md5-string-2">
          <xsl:with-param name="state">
            <xsl:call-template name="md5-init"/>
          </xsl:with-param>
          <xsl:with-param name="string" select="$string"/>
          <xsl:with-param name="i" select="0"/>
          <xsl:with-param name="size" select="0"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="md5-byte">
          <xsl:with-param name="state">
            <xsl:call-template name="md5-init"/>
          </xsl:with-param>
          <xsl:with-param name="byte" select="$byte"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
