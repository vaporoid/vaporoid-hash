#include <stdint.h>
#include <stdlib.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

inline void print_table(std::ostream& out, uint32_t n, const std::string& indent) {
  out << indent << "<table>\n";
  for (uint32_t i = 0; i < n; ++i) {
    out << indent << "  <item key=\"" << i << "\">" << std::hex << std::setfill('0') << std::setw(4) << i << std::dec << std::setfill(' ') << "</item>\n";
  }
  out << indent << "</table>\n";
}

inline void print_template_ident(std::ostream& out, const std::string& indent) {
  out << indent << "<xsl:value-of select=\"$i\"/>\n";
}

inline void print_template_choose_linear(std::ostream& out, uint32_t i, uint32_t n, const std::string& indent) {
  out << indent << "<xsl:choose>\n";
  for (; i < n; ++i) {
    out << indent << "  <xsl:when test=\"$i = " << i << "\">\n"
        << indent << "    <xsl:text>" << std::hex << std::setfill('0') << std::setw(4) << i << std::dec << std::setfill(' ') << "</xsl:text>\n"
        << indent << "  </xsl:when>\n";
  }
  out << indent << "</xsl:choose>\n";
}

inline void print_template_choose_binary(std::ostream& out, uint32_t i, uint32_t n, const std::string& indent) {
  if (n == 1) {
    out << indent << "<xsl:text>" << std::hex << std::setfill('0') << std::setw(4) << i << std::dec << std::setfill(' ') << "</xsl:text>\n";
  } else {
    uint32_t m = n / 2;
    out << indent << "<xsl:choose>\n"
        << indent << "  <xsl:when test=\"$i &lt; " << (i + m) << "\">\n";
    print_template_choose_binary(out, i, m, indent + "    ");
    out << indent << "  </xsl:when>\n"
        << indent << "  <xsl:otherwise>\n";
    print_template_choose_binary(out, i + m, n - m, indent + "    ");
    out << indent << "  </xsl:otherwise>\n"
        << indent << "</xsl:choose>\n";
  }
}

inline void print_template_lookup(std::ostream& out, const std::string& href, const std::string& indent) {
  out << indent << "<xsl:for-each select=\"document('" << href << "')\">\n"
      << indent << "  <xsl:value-of select=\"key('table-key', string($i))\"/>\n"
      << indent << "</xsl:for-each>\n";
}

inline void print_template(std::ostream& out, const std::string& name, uint32_t n, const std::string& indent) {
  out << indent << "<xsl:template name=\"" << name << "\">\n"
      << indent << "  <xsl:param name=\"i\"/>\n";
  if (name == "ident") {
    print_template_ident(out, indent + "  ");
  } else if (name == "choose-linear") {
    print_template_choose_linear(out, 0, n, indent + "  ");
  } else if (name == "choose-binary") {
    print_template_choose_binary(out, 0, n, indent + "  ");
  } else if (name == "lookup-internal") {
    print_template_lookup(out, "", indent + "  ");
  } else if (name == "lookup-external") {
    std::ostringstream o;
    o << "table-lookup-external-" << n << "_.xml";
    print_template_lookup(out, o.str(), indent + "  ");
  }
  out << indent << "</xsl:template>\n";
}

inline void print_xsl(std::ostream& out, const std::string& name, size_t n) {
  out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      << "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n";
  if (name == "lookup-internal" || name == "lookup-external") {
    out << "  <xsl:key name=\"table-key\" match=\"item\" use=\"@key\"/>\n";
  }
  out << "  <xsl:output method=\"text\"/>\n";
  if (name == "lookup-internal") {
    out << "  <xsl:variable name=\"table\">\n";
    print_table(out, n, "    ");
    out << "  </xsl:variable>\n";
  }
  print_template(out, name, n, "  ");

  std::ostringstream o;
  o << std::hex << std::setfill('0');
  for (uint32_t i = n - 100; i < n; ++i) {
    o << std::setw(4) << i;
  }

  out << "  <xsl:template name=\"each\">\n"
      << "    <xsl:param name=\"i\"/>\n"
      << "    <xsl:param name=\"n\"/>\n"
      << "    <xsl:call-template name=\"" << name << "\">\n"
      << "      <xsl:with-param name=\"i\" select=\"$i\"/>\n"
      << "    </xsl:call-template>\n"
      << "    <xsl:if test=\"$i &lt; $n\">\n"
      << "      <xsl:call-template name=\"each\">\n"
      << "        <xsl:with-param name=\"i\" select=\"$i + 1\"/>\n"
      << "        <xsl:with-param name=\"n\" select=\"$n\"/>\n"
      << "      </xsl:call-template>\n"
      << "    </xsl:if>\n"
      << "  </xsl:template>\n"
      << "  <xsl:template match=\"/\">\n"
      << "    <xsl:variable name=\"result\">\n"
      << "      <xsl:call-template name=\"each\">\n"
      << "        <xsl:with-param name=\"i\" select=\"" << n - 100 << "\"/>\n"
      << "        <xsl:with-param name=\"n\" select=\"" << n -   1 << "\"/>\n"
      << "      </xsl:call-template>\n"
      << "    </xsl:variable>\n"
      << "    <xsl:choose>\n"
      << "      <xsl:when test=\"$result = '" << o.str() << "'\">\n"
      << "        <xsl:text>[PASS]</xsl:text>\n"
      << "      </xsl:when>\n"
      << "      <xsl:otherwise>\n"
      << "        <xsl:text>[FAIL]</xsl:text>\n"
      << "      </xsl:otherwise>\n"
      << "    </xsl:choose>\n"
      << "    <xsl:text> " << name << "\n"
      << "</xsl:text>\n"
      << "  </xsl:template>\n"
      << "</xsl:stylesheet>\n";
}

inline void print_xml(std::ostream& out, const std::string& name, size_t n) {
  out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      << "<?xml-stylesheet type=\"text/xsl\" href=\"table-" << name << "-" << n << ".xsl\"?>\n"
      << "<table/>\n";
}

inline void print_ext(std::ostream& out, size_t n) {
  out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  print_table(out, n, "");
}

int main(int argc, char* argv[]) {
  std::string type = argv[1];
  std::string name = argv[2];
  uint32_t n = atoi(argv[3]);

  if (type == "xsl") {
    print_xsl(std::cout, name, n);
  } else if (type == "xml") {
    print_xml(std::cout, name, n);
  } else {
    print_ext(std::cout, n);
  }
  return 0;
}
