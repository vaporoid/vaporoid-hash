<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="table_.xsl"/>

  <xsl:output method="text"/>

  <xsl:variable name="endl">
    <xsl:text>
</xsl:text>
  </xsl:variable>

  <xsl:template name="no-lookup">
    <xsl:param name="i"/>
    <xsl:param name="n"/>
    <xsl:text>xxxx</xsl:text>
    <xsl:if test="$i &lt;= $n">
      <xsl:call-template name="no-lookup">
        <xsl:with-param name="i" select="$i + 1"/>
        <xsl:with-param name="n" select="$n"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="choose-linear">
    <xsl:param name="i"/>
    <xsl:param name="n"/>
    <xsl:call-template name="table-choose-linear">
      <xsl:with-param name="i" select="$i"/>
    </xsl:call-template>
    <xsl:if test="$i &lt;= $n">
      <xsl:call-template name="choose-linear">
        <xsl:with-param name="i" select="$i + 1"/>
        <xsl:with-param name="n" select="$n"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="choose-binary">
    <xsl:param name="i"/>
    <xsl:param name="n"/>
    <xsl:call-template name="table-choose-binary">
      <xsl:with-param name="i" select="$i"/>
    </xsl:call-template>
    <xsl:if test="$i &lt;= $n">
      <xsl:call-template name="choose-binary">
        <xsl:with-param name="i" select="$i + 1"/>
        <xsl:with-param name="n" select="$n"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="position">
    <xsl:param name="i"/>
    <xsl:param name="n"/>
    <xsl:comment>
      <xsl:call-template name="table-position"/>
    </xsl:comment>
    <xsl:value-of select="//table/item[1]"/>
    <xsl:if test="$i &lt;= $n">
      <xsl:call-template name="position">
        <xsl:with-param name="i" select="$i + 1"/>
        <xsl:with-param name="n" select="$n"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="id">
    <xsl:param name="i"/>
    <xsl:param name="n"/>
    <xsl:comment>
      <xsl:value-of select="id('id0')"/>
    </xsl:comment>
    <xsl:if test="$i &lt;= $n">
      <xsl:call-template name="id">
        <xsl:with-param name="i" select="$i + 1"/>
        <xsl:with-param name="n" select="$n"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="key">
    <xsl:param name="i"/>
    <xsl:param name="n"/>
    <xsl:call-template name="table-key">
      <xsl:with-param name="i" select="$i"/>
    </xsl:call-template>
    <xsl:if test="$i &lt;= $n">
      <xsl:call-template name="key">
        <xsl:with-param name="i" select="$i + 1"/>
        <xsl:with-param name="n" select="$n"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="/">
    <xsl:call-template name="key">
      <xsl:with-param name="i" select="60000"/>
      <xsl:with-param name="n" select="60099"/>
    </xsl:call-template>
    <xsl:value-of select="$endl"/>

  </xsl:template>
</xsl:stylesheet>
