<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text"/>

  <xsl:variable name="endl">
    <xsl:text>
</xsl:text>
</xsl:variable>

  <xsl:template match="/">
    <xsl:value-of select="concat('xsl:version = ', system-property('xsl:version'), $endl)"/>
    <xsl:value-of select="concat('xsl:vendor = ', system-property('xsl:vendor'), $endl)"/>
    <xsl:value-of select="concat('xsl:vendor-url = ', system-property('xsl:vendor-url'), $endl)"/>
  </xsl:template>
</xsl:stylesheet>
