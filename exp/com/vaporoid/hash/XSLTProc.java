package com.vaporoid.hash;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;

class XSLTProc {
  public static void main(String args[]) throws Exception {
    DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    Document document = documentBuilder.parse(new File(args[1]));
    Transformer transformer = TransformerFactory.newInstance().newTransformer(new StreamSource(new File(args[0])));
    transformer.transform(new DOMSource(document), new StreamResult(System.out));
  }
};
