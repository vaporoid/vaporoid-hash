<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text"/>

  <xsl:variable name="endl">
    <xsl:text>
</xsl:text>
  </xsl:variable>

  <xsl:template name="string">
    <xsl:param name="string" select="'9'"/>
    <xsl:param name="n" select="18"/>

    <xsl:value-of select="concat('string(number(', $string, ')) = ', string(number(string($string))), $endl)"/>
    <xsl:if test="string-length($string) &lt; $n">
      <xsl:call-template name="string">
        <xsl:with-param name="string" select="concat($string, '9')"/>
        <xsl:with-param name="n" select="$n"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="format-number">
    <xsl:param name="string" select="'9'"/>
    <xsl:param name="n" select="18"/>

    <xsl:value-of select="concat('format-number(number(', $string, ')) = ', format-number(number(string($string)), '0'), $endl)"/>
    <xsl:if test="string-length($string) &lt; $n">
      <xsl:call-template name="format-number">
        <xsl:with-param name="string" select="concat($string, '9')"/>
        <xsl:with-param name="n" select="$n"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="/">
    <xsl:call-template name="format-number">
      <xsl:with-param name="string" select="'0'"/>
      <xsl:with-param name="n" select="0"/>
    </xsl:call-template>
    <xsl:call-template name="string"/>
    <xsl:call-template name="format-number"/>
    <xsl:call-template name="format-number">
      <xsl:with-param name="string" select="'2251799813685247'"/> <!-- 0x007fffffffffffff -->
      <xsl:with-param name="n" select="0"/>
    </xsl:call-template>
    <xsl:call-template name="format-number">
      <xsl:with-param name="string" select="'4503599627370495'"/> <!-- 0x00ffffffffffffff -->
      <xsl:with-param name="n" select="0"/>
    </xsl:call-template>
    <xsl:call-template name="format-number">
      <xsl:with-param name="string" select="'9007199254740991'"/> <!-- 0x01ffffffffffffff -->
      <xsl:with-param name="n" select="0"/>
    </xsl:call-template>
  </xsl:template>
</xsl:stylesheet>
