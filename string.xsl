<?xml version="1.0" encoding="UTF-8"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements. See the NOTICE file distributed with this
  work for additional information regarding copyright ownership. The ASF
  licenses this file to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  License for the specific language governing permissions and limitations under
  the License.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:key name="vaporoid-hash-table" match="item" use="@key"/>

  <xsl:template name="string-encode">
    <xsl:param name="string"/>
    <xsl:param name="i" select="1"/>
    <xsl:param name="n" select="string-length($string)"/>

    <xsl:choose>
      <xsl:when test="$n = 0"/>
      <xsl:when test="$n = 1">
        <xsl:for-each select="document('vaporoid-hash-char-encode-1.xml')">
          <xsl:variable name="byte1" select="key('vaporoid-hash-table', substring($string, $i, 1))"/>
          <xsl:choose>
            <xsl:when test="boolean($byte1)">
              <xsl:value-of select="$byte1"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:for-each select="document('vaporoid-hash-char-encode-2.xml')">
                <xsl:variable name="byte2" select="key('vaporoid-hash-table', substring($string, $i, 1))"/>
                <xsl:choose>
                  <xsl:when test="boolean($byte2)">
                    <xsl:value-of select="$byte2"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:for-each select="document('vaporoid-hash-char-encode-3.xml')">
                      <xsl:variable name="byte3" select="key('vaporoid-hash-table', substring($string, $i, 1))"/>
                      <xsl:choose>
                        <xsl:when test="boolean($byte3)">
                          <xsl:value-of select="$byte3"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:message>Unknown char</xsl:message>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:for-each>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="m" select="floor($n div 2)"/>
        <xsl:call-template name="string-encode">
          <xsl:with-param name="string" select="$string"/>
          <xsl:with-param name="i" select="$i"/>
          <xsl:with-param name="n" select="$m"/>
        </xsl:call-template>
        <xsl:call-template name="string-encode">
          <xsl:with-param name="string" select="$string"/>
          <xsl:with-param name="i" select="$i + $m"/>
          <xsl:with-param name="n" select="$n - $m"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="string-add">
    <xsl:param name="a"/>
    <xsl:param name="b"/>
    <xsl:param name="c"/>
    <xsl:param name="d"/>

    <xsl:value-of select="concat($a, $b, $c, $d)"/>
  </xsl:template>

  <xsl:template name="string-fill">
    <xsl:param name="char"/>
    <xsl:param name="n"/>

    <xsl:choose>
      <xsl:when test="$n = 0"/>
      <xsl:when test="$n = 1">
        <xsl:value-of select="$char"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="m" select="floor($n div 2)"/>
        <xsl:call-template name="string-fill">
          <xsl:with-param name="char" select="$char"/>
          <xsl:with-param name="n" select="$m"/>
        </xsl:call-template>
        <xsl:call-template name="string-fill">
          <xsl:with-param name="char" select="$char"/>
          <xsl:with-param name="n" select="$n - $m"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
